﻿Modified
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Test1
    {
        static void Main()
        {
            //string fname = "Prasad";
            //string lname = "Reddy";
            //Console.WriteLine("Welcome "+fname +" "+lname);
            //Console.WriteLine("Welcome {0} {1}",fname,lname);

            //string str = "welcome to .Net class";
            //string str1 = str.Substring(0, 7);

            //Console.WriteLine(str1);

            //string names = "prasad,reddy,harshith,hanvi";
            //string[] fnames = names.Split(',');
            //foreach(string name in fnames)
            //{
            //    Console.WriteLine(name);
            //}


            //string str = Console.ReadLine();
            //if (str.Contains(".Net"))
            //{
            //    Console.WriteLine("exist");
            //}
            //else
            //{
            //    Console.WriteLine("does not exist");
            //}

            //string fname = Console.ReadLine();
            //string lname = Console.ReadLine();
            //Console.WriteLine(fname.Trim(',') + lname);

            ////boxing 
            //int i = 20;
            //string str = string.Empty;
            //str = i.ToString();
            //Console.WriteLine(str);


            ////unboxing

            //string str1 = "30";
            //int j = Convert.ToInt32(str1);

            //string concatination
            //string str = "my";
            //str = str + " name";
            //str = str + " is ";
            //str = str + " Prasad";
            //Console.WriteLine(str);

            //string builder
            //StringBuilder sb = new StringBuilder();            
            //sb.Append("My");
            //sb.Append(" Name");
            //sb.Append(" is");
            //Console.WriteLine(sb.ToString());

            ArrayList names = new ArrayList();
            names.Add("Prasad");
            names.Add("Reddy");
            
            string str =(string)names[2];
            Console.ReadKey();

        }
    }
}
